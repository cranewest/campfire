<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="small-12 medium-12 large-12 columns">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						echo '<div class="row"><h2 class="page-title center">'.get_the_title().'</h2></div>';
						// comments_template( '', true );
					}
				}
			?>
		</div>
		<div class="small-12 medium-8 large-8 columns page_cont_padding">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						the_content();
						// comments_template( '', true );
					}
				}
			?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>