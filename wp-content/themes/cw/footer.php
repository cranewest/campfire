<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
<div class="footer_bg">
	<footer role="contentinfo" class="row">
		<div class="small-12 medium-6 large-6 columns footer_border_padding">
			<p>
				<a href="/history/">About Us</a><br>
				<a href="/after-school/">After School</a><br>
				<a href="/employment/">Employment</a><br>
				<a href="/rental/"> Rental</a><br>
				<a href="/calendar/">Calendar</a><br>
				<a href="/contact/">Contact</a>
			</p>
			<a href="http://www.handstohandsfund.org/"><img class="hh_logo"src="<?php bloginfo('template_directory'); ?>/img/handstohandsfund.png" ></a>
			
			
		</div>
		<div class="small-12 medium-6 large-6 columns footer_border_padding">
			<div class="f_right">
				<img class="campfire_logo_footer" src="<?php bloginfo('template_directory'); ?>/img/logo_footer.png" alt="<?php bloginfo( 'name' ); ?>" ><br><br>
				<?echo '<a class="phone_footer" href="tel:'.preg_replace("/[^0-9]/","",cw_options_get_option( 'cwo_phone' )).'">'.cw_options_get_option( 'cwo_phone' ).'</a>';?><br><br>
				<a class="cw" href="http://crane-west.com/"><img src="<?php echo get_template_directory_uri(); ?>/img/cw-dark.png" alt="Site Powered by Crane | West" /></a>
				<p>&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?></p>
			</div>
		</div>
	</footer>
</div>
	<!-- WP_FOOTER() -->
	<?php wp_footer(); ?>
	<?php
		// must activate CW Options Page plugin for the line below to work
		$ga_code = cw_options_get_option('cwo_ga'); if( !empty($ga_code) ) {
	?>
	<script type="text/javascript">
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?php echo $ga_code; ?>', 'auto');
		ga('send', 'pageview');
	</script>
	<?php } ?>
</body>
</html>