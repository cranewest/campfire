<?php
	if(is_404() || is_search())
		return;

	global $post;
	$page_id = $post->ID;
	
	$slides_args = array(
		'post_type' => 'slides',
		'posts_per_page' => -1,
		'meta_key' => '_cwmb_slide_page',
		'meta_value' => $page_id,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	$the_slides = new WP_Query($slides_args);

	// echo_pre($page_id);

	if($the_slides->have_posts()){
		echo '<div class="cw-slideshow">';
			echo '<ul class="styleless cw-slider">';
			while($the_slides->have_posts()) {
				$the_slides->the_post();

				$slide_title = get_post_meta($post->ID, '_cwmb_slide_title', true);
				$slide_subtitle = get_post_meta($post->ID, '_cwmb_slide_subtitle', true);
				$slide_title_pos = get_post_meta($post->ID, '_cwmb_slide_title_pos', true);
				$slide_image = get_post_meta($post->ID, '_cwmb_slide_image', true);
				$slide_image_pos = get_post_meta($post->ID, '_cwmb_slide_image_pos', true);
				$slide_caption = get_post_meta($post->ID, '_cwmb_slide_caption', true);
				$slide_caption_pos = get_post_meta($post->ID, '_cwmb_slide_caption_pos', true);
				$slide_link = get_post_meta($post->ID, '_cwmb_slide_link', true);
				$slide_color = get_post_meta($post->ID, '_cwmb_slide_color', true);

				$slide_mp4 = get_post_meta($post->ID, '_cwmb_slide_mp4', true);
				$slide_webm = get_post_meta($post->ID, '_cwmb_slide_webm', true);

				$video = false;
				$class = 'slide';
				if(!empty($slide_webm) && !empty($slide_mp4)) {
					$video = true;
					$class .= ' show-for-medium-up';
				}

				$cropped = aq_resize( $slide_image, 600, 450, true, true, true );

				// if(empty($slide_color)) {
				// 	$slide_color = '#0075bf';
				// }

				echo '<li class="'.$class.'"><div class="inner small-12 medium-12 large-12 columns">';
					if(!empty($slide_image)) { echo '<img src="'.$cropped.'" alt="" />'; }

					$class = '';
					if(!empty($slide_title) || !empty($slide_caption)) {
						$class = 'has-cap';
					}

					echo '<div class="slide-words '.$class.'">';
						if(!empty($slide_caption)) {
							echo '<p class="slide-caption align-'.$slide_caption_pos.'">';
								echo nl2br($slide_caption);
								if(!empty($slide_link)) { echo '<br><a class="button small" href="'.$slide_link.'">More Info</a>'; }
							echo '</p>';
						}
					echo '</div>';
				echo '</div></li>';
			}
		echo '</ul>';
	echo '</div>'; // cw-slideshow
	}
wp_reset_query(); ?>