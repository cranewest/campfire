<?php
/**
 * Template Name: Staff Page 
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="small-12 medium-12 large-12 columns">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						echo '<div class="row"><h2 class="page-title center">'.get_the_title().'</h2></div>';
						// comments_template( '', true );
					}
				}
			?>
		</div>
		<div class="small-12 medium-8 large-8 columns page_cont_padding">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						the_content();
						// comments_template( '', true );
					}
				}
			?>


			<?php
				$post_type = 'staff';
				$taxonomies = get_object_taxonomies( (object) array( 'post_type' => $post_type) );
				foreach( $taxonomies as $taxonomy ) {

				// get category terms
				$terms = get_terms( $taxonomy );
				foreach( $terms as $term ) { ?>
					<!-- display the terms -->
					<h2 class="center"><?php echo $term->name ?></h2><hr>
					<div>
					<!-- get the posts -->
					<?php
						$args = array(
							'post_type' => $post_type,
							'posts_per_page' => -1,
							'orderby' => 'title',
							'order' => 'ASC',
							'tax_query' => array(
								array(
									'taxonomy' => $taxonomy,
									'field' => 'slug',
									'terms' => $term
								)
							)
						);
						$posts = new WP_Query( $args );
						if( $posts->have_posts() ) {
							while( $posts->have_posts() ) {
								$posts->the_post();
								
								echo'<div class = "row">';
							$image_link = get_post_meta($post->ID, '_cwmb_staff_image', true);
								?>
								<div class="small-12 medium-6 large-6 columns center">
									<?	if ($image_link){ ?>
										<img src="<?echo $image_link;?>" alt="Smiley face" height="120" width="120">
									<? }
										else { ?>
										<img src="<?php bloginfo('template_directory'); ?>/img/placeholder.jpg" alt="Smiley face" height="120" width="120">
									<? } ?>
									<h5 class="h_grey"><? echo get_post_meta($post->ID, '_cwmb_f_name', true).' '.get_post_meta($post->ID, '_cwmb_l_name', true); ?></h5>
									<h5 class="h_grey"><? echo get_post_meta($post->ID, '_cwmb_staff_title', true); ?></h5>
									<h5 class=""><a href="mailto:<? echo get_post_meta($post->ID, '_cwmb_staff_email', true); ?>"><? echo get_post_meta($post->ID, '_cwmb_staff_email', true); ?></a></h5>
								</div>
								<div class="small-12 medium-6 large-6 columns"><h6 class="h_grey"><? echo wpautop( get_post_meta( get_the_ID(), '_cwmb_staff_bio', true ) ); ?></h6></div>
								<?
							echo'</div><hr>';

							 } // end while have_posts
						} // end if have_posts ?>
					</div>
				<?php } // end foreach $terms
			} // end foreach $taxonomies
			wp_reset_query(); ?>



		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>