<?php
/**
 * Template Name: Calendar Page 
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="small-12 medium-12 large-12 columns">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						echo '<div class="row"><h2 class="page-title center">Calendar</h2></div>';
						the_content();
						// comments_template( '', true );
					}
				}
			?>
		</div>

		<?php //get_sidebar(); ?>
	</div>

<?php get_footer(); ?>