<?php
/**
 * Template Name: Home Page 
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	
	<div class="main row" role="main">
		<!-- <div class="home_slider"> -->
			<!-- <div class="small-12 medium-12 large-12 columns home_slider_image"> -->
				<?php //get_template_part('content', 'slides'); ?>
			<!-- </div> -->
			<!-- <div class="small-12 medium-4 large-4 columns home_sidebar">
					
			</div> -->
		<!-- </div> -->
		<div class="small-12 medium-12 large-12 columns home_content_header">
			<h3 class="icon_bin">
				<a href="https://www.facebook.com/CampFireNorthTexas"><i class="fa fa-facebook-square"></i></a>
				<a href="https://instagram.com/campfirenorthtexas"><i class="fa fa-instagram "></i></a>
				<a href="https://twitter.com/CampFireNTX"><i class="fa fa-twitter-square "></i></a>
				
			</h3>
		</div>

		<div class="small-12 medium-12 large-12 columns">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php //get_template_part( 'content', 'page' ); ?>
				<?php // comments_template( '', true ); ?>
				<?php //the_content(); ?>
			<?php endwhile; // end of the loop. ?>
		</div>

		<div class="small-12 medium-12 large-12 columns">
			<?php 	
				//adjusting the query
				$args = array(
					// 'category_name' => 'news',
					'post_type' => 'post',
					'posts_per_page' => 3,
					'order' => DESC
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					echo'<div class = "small-12 medium-4 large-4 columns "><h5 class="page-title center">Latest News</h5><br>';
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						echo'<div class="news_article">';

							?><h6 class="h_grey"><a href="<? the_permalink(); ?>"><? echo get_the_title(); ?></a></h6><br><?
							
			
							echo'<div class="act_content">';
								the_excerpt();
							echo'</div>';
							
								?><a class="f_right" href="<?php the_permalink(); ?>">Read More</a><br><?
							
						echo'</div>';
						
					}
					echo'</div>';
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
			<div class="small-12 medium-4 large-4 columns">
				<h5 class="page-title center">Upcoming Events</h5><br>
				<?

					$events = tribe_get_events(array(
						'eventDisplay'=>'all',
						'orderby' => 'EventStartDate',
						'posts_per_page'=>5,
						'orderby' => 'EventStartDate',
						'order' => 'DESC'
					 ));

					$events = array_reverse($events);
					$events = array_slice($events, 0, 5);
					if(!empty($events)) {
						foreach($events as $post) {
							setup_postdata($post);

							echo'<div class="news_article">';

								?><h6 class="h_grey"><a href="<? the_permalink(); ?>"><? echo get_the_title(); ?></a></h6><br><?
								echo sp_get_start_date().'<br><br>';
								
				
								// echo'<div class="act_content">';
								// 	the_excerpt();
								// echo'</div>';
								
									?><a class="f_right" href="<?php the_permalink(); ?>">Read More</a><br><?
								
							echo'</div>';
						}
					} else {
						echo '<div class="news_article"><p>No upcoming events at this time. Check back soon!</p></div>';
					}

				?>
			</div> 

			<div class="small-12 medium-4 large-4 columns">
				<h5 class="page-title center">From Facebook</h5><br>
				<?php echo do_shortcode('[custom-facebook-feed]'); ?>
			</div> 

		</div>

		<div class="small-12 medium-12 large-12 columns promo_div">
			<?php 	
				//adjusting the query
				$args = array(
					'post_type' => 'promos',
					'posts_per_page' => 2,
					'order' => DESC
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						$link = get_post_meta($post->ID, '_cwmb_promo_link', true);
						$img_src = get_post_meta($post->ID, '_cwmb_promo_image', true);
				
						
						echo'<div class="small-12 medium-6 large-6 columns promo">';
						if ($link){
							echo'<a target="_blank" href="'.$link.'">'; 
							?><img class="promo" src="<? echo $img_src; ?>"> <?
							echo'</a>';
						} 
						else{
							?><img class="promo" src="<? echo $img_src; ?>"> <?
						}
						echo'</div>';
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
		
		</div>

		<?php //get_sidebar(); ?>
	</div>

<?php get_footer(); ?>