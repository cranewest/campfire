<?php
/**
 * Template Name: Board Page 
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<!-- <div class="small-12 medium-12 large-12 columns">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						echo '<h2 class="page-title center">'.get_the_title().'</h2>';
						the_content();
						// comments_template( '', true );
					}
				}
			?>
		</div> -->
		<div class="small-12 medium-8 large-8 columns">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						echo '<h2 class="page-title center">'.get_the_title().'</h2>';
						the_content();
						// comments_template( '', true );
					}
				}
			?>
			<?php 
				//adjusting the query
				$args = array(
					'post_type' => 'staff',
					'posts_per_page' => -1,
					// 'orderby' => 'menu_order'
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						$p =  get_post_meta($post->ID, '_cwmb_member_radio', true);
						if ('board' == $p) {
							echo'<div class = "row">';
							$image_link = get_post_meta($post->ID, '_cwmb_staff_image', true);
								?>
								<div class="small-12 medium-6 large-6 columns center">
									<?	if ($image_link){ ?>
										<img src="<?echo $image_link;?>" alt="Smiley face" height="120" width="120">
									<? }
										else { ?>
										<img src="<?php bloginfo('template_directory'); ?>/img/placeholder.jpg" alt="Smiley face" height="120" width="120">
									<? } ?>
									<h5 class="h_grey"><? echo get_post_meta($post->ID, '_cwmb_f_name', true).' '.get_post_meta($post->ID, '_cwmb_l_name', true); ?></h5>
									<h5 class="h_grey"><? echo get_post_meta($post->ID, '_cwmb_staff_title', true); ?></h5>
									<h5 class=""><a href="mailto:<? echo get_post_meta($post->ID, '_cwmb_staff_email', true); ?>"><? echo get_post_meta($post->ID, '_cwmb_staff_email', true); ?></a></h5>
								</div>
								<div class="small-12 medium-6 large-6 columns"><h6 class="h_grey"><? echo wpautop( get_post_meta( get_the_ID(), '_cwmb_staff_bio', true ) ); ?></h6></div>
								<?
							echo'</div><hr>';

						}
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>



		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>